﻿using NUnit.Framework;
using QuizOfTheDay.Database;

namespace QuizOfTheDay.Tests
{
    [TestFixture]
    [Ignore]
    public class QuizRepositoryTests
    {
        [Test]
        [Ignore]
        public void ShouldSaveAndLoadQuiz()
        {
            var quiz = TestData.GetStandardQuiz();
            var repository = new QuizRepository();
            quiz = repository.Add(quiz);
            repository.Remove(quiz);
            var shouldNotBeThere = repository.Get(quiz.Id);
            Assert.IsNull(shouldNotBeThere);
        }
    }
}