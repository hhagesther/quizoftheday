﻿using QuizOfTheDay.Domain;

namespace QuizOfTheDay.Tests
{
    internal class TestData
    {
        internal static Quiz GetStandardQuiz()
        {
            var quiz = new Quiz(QuizType.OneCorrectAnswer);
            quiz.Questions.Enqueue(GetTestQuestionOne());
            quiz.Questions.Enqueue(GetTestQuestionTwo());
            quiz.Questions.Enqueue(GetTestQuestionThree());
            return quiz;
        }

        private static Question GetTestQuestionOne()
        {
            var question = new Question {Text = "Hva er meningen med livet?"};
            question.Alternatives.Add(new Alternative("42", 1, question));
            question.Alternatives.Add(new Alternative("Å leve", 0, question));
            question.Alternatives.Add(new Alternative("Å formere seg", 0, question));
            return question;
        }

        private static Question GetTestQuestionTwo()
        {
            var question = new Question { Text = "Hva heter hovedstaden i Norge?" };
            question.Alternatives.Add(new Alternative("Oslo", 1, question));
            question.Alternatives.Add(new Alternative("Bergen", 0, question));
            question.Alternatives.Add(new Alternative("Stockholm", 0, question));
            return question;
        }

        private static Question GetTestQuestionThree()
        {
            var question = new Question { Text = "Hva tror du om dette?" };
            question.Alternatives.Add(new Alternative("Masse", 0, question));
            question.Alternatives.Add(new Alternative("Ingenting", 0, question));
            question.Alternatives.Add(new Alternative("???", 1, question));
            return question;
        }
    }
}