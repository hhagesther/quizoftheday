﻿using NUnit.Framework;
using QuizOfTheDay.Domain;
using QuizOfTheDay.Domain.Events;
using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Tests
{
    [TestFixture]
    public class QuizControllerTests
    {
        [SetUp]
        public void SetUp()
        {
            DomainEvents.ClearCallbacks();
        }

        [Test]
        public void QuizStarted_ShouldFireQuestionAsked()
        {
            var quizController = new QuizController();
            Question question = null;
            DomainEvents.Register<QuestionAsked>(qa => question = qa.Question);
            DomainEvents.Raise(new QuizSelected {Quiz = TestData.GetStandardQuiz()});
            Assert.IsNotNull(question, "When a new quiz is selected, the first question of the new quiz should automatically be asked.");
        }

        [Test]
        public void QuestionAnswered_ShouldFireQuestionAsked()
        {
            Question question = null;
            DomainEvents.Register<QuestionAsked>(qa => question = qa.Question);
            bool hasAnswered = false;
            DomainEvents.Register<QuestionAnswered>(qa => hasAnswered = true);

            var quizController = new QuizController();
            var player = new Player("Halvard");
            DomainEvents.Raise(new QuizSelected {Quiz = TestData.GetStandardQuiz()});
            player.Answer(question.Alternatives[0]);

            Assert.IsTrue(hasAnswered);
            Assert.AreEqual("Hva heter hovedstaden i Norge?", question.Text);
        }

        [Test]
        public void AllQuestionAnswered_ShouldFireQuizFinished()
        {
            bool hasFinished = false;
            DomainEvents.Register<QuizFinished>(qf => hasFinished = true);

            var quizController = new QuizController();
            var player = new Player("Halvard");
            Question question = null;
            DomainEvents.Register<QuestionAsked>(qa => question = qa.Question);
            DomainEvents.Raise(new QuizSelected { Quiz = TestData.GetStandardQuiz() });
            player.Answer(question.Alternatives[0]);
            player.Answer(question.Alternatives[0]);
            player.Answer(question.Alternatives[0]);
            
            Assert.IsTrue(hasFinished);
        }

        [Test]
        public void AllQuestionsAnswered_ShouldCalculateResult()
        {
            QuizFinished quizFinished = null;
            DomainEvents.Register<QuizFinished>(qf => quizFinished = qf);

            var quizController = new QuizController();
            var player = new Player("Halvard");
            Question question = null;
            DomainEvents.Register<QuestionAsked>(qa => question = qa.Question);
            DomainEvents.Raise(new QuizSelected { Quiz = TestData.GetStandardQuiz() });
            player.Answer(question.Alternatives[0]);
            player.Answer(question.Alternatives[0]);
            player.Answer(question.Alternatives[0]);

            Assert.AreEqual(2, quizFinished.Score);
        }
    }
}