using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using QuizOfTheDay.Domain;


namespace QuizOfTheDay.Database
{
    public  class QuizRepository
    {
        private readonly MongoCollection<Quiz> _quizzes; 

        public QuizRepository()
        {
            const string connectionString = "mongodb://10.1.4.175";
            var client = new MongoClient(connectionString);
            var server = client.GetServer();
            var database = server.GetDatabase("test");
            _quizzes = database.GetCollection<Quiz>("quizzes");
        }

        public Quiz Add(Quiz quiz)
        {
            _quizzes.Insert(quiz);
            return quiz;
        }

        public Quiz Get(ObjectId objectId)
        {
            var query = Query<Quiz>.EQ(e => e.Id, objectId);
            var quiz = _quizzes.FindOne(query);
            return quiz;
        }

        public void Edit(Quiz quiz)
        {
            _quizzes.Save(quiz);
        }

        public void Remove(Quiz quiz)
        {
            var query = Query<Quiz>.EQ(e => e.Id, quiz.Id);
            _quizzes.Remove(query);
        }
    }
}