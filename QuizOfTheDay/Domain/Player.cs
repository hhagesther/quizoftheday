﻿using System;
using QuizOfTheDay.Domain.Events;
using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain
{
    public class Player
    {
        public string Id { get; private set; }
        public string Name { get; private set; }
        public Country Country { get; set; }

        public Player(string name)
        {
            Name = name;
            Id = Guid.NewGuid().ToString();
        }

        public void Answer(Alternative alternativeChosen)
        {
            DomainEvents.Raise(new QuestionAnswered
                                   {
                                       AlternativeChosen = alternativeChosen,
                                       Player = this
                                   });
        }
    }
}
