﻿namespace QuizOfTheDay.Domain
{
    public enum Country
    {
        Australia,
        Canada,
        China,
        India,
        Malaysia,
        Norway,
        Pakistan,
        Philippines,
        UK,
        USA
    }
}