﻿using QuizOfTheDay.Domain.Events;
using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain
{
    public class QuizController
    {
        private Quiz _quiz;
        private int _score;

        public QuizController()
        {
            DomainEvents.Register<QuizSelected>(QuizSelected);
            DomainEvents.Register<QuestionAnswered>(QuestionAnswered);
        }

        private void QuestionAnswered(QuestionAnswered questionAnswered)
        {
            _score += questionAnswered.AlternativeChosen.Score;
            if (_quiz.Questions.Count > 0)
            {
                DomainEvents.Raise(new QuestionAsked { Question = _quiz.Questions.Dequeue() });
            }
            else
            {
                DomainEvents.Raise(new QuizFinished
                                       {
                                           Player = questionAnswered.Player,
                                           Quiz = _quiz,
                                           Score = _score
                                       });
            }
        }

        private void QuizSelected(QuizSelected quizSelected)
        {
            _quiz = quizSelected.Quiz;
            _score = 0;
            DomainEvents.Raise(new QuestionAsked {Question = _quiz.Questions.Dequeue()});
        }
    }
}
