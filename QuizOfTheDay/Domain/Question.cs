﻿using System.Collections.Generic;

namespace QuizOfTheDay.Domain
{
    public class Question
    {
        public string Text { get; set; }
        public List<Alternative> Alternatives { get; set; }

        public Question()
        {
            Alternatives = new List<Alternative>();
        }
    }
}