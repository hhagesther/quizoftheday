﻿namespace QuizOfTheDay.Domain
{
    public enum QuizType
    {
        NoCorrectAnswers,
        OneCorrectAnswer,
        MultipleCorrectAnswers,
        ScorePerAnswer
    }
}