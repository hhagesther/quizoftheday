﻿using System.Collections.Generic;
using MongoDB.Bson;

namespace QuizOfTheDay.Domain
{
    public class Quiz
    {
        public ObjectId Id { get; set; }
        public Queue<Question> Questions { get; private set; }
        public QuizType QuizType { get; private set; }

        public Quiz(QuizType quizType)
        {
            QuizType = quizType;
            Questions = new Queue<Question>();
        }
    }
}