﻿using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain.Events
{
    public class QuestionAnswered : IDomainEvent
    {
        public Alternative AlternativeChosen { get; set; }
        public Player Player { get; set; }
    }
}