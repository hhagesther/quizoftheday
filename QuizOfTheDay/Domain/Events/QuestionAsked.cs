﻿using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain.Events
{
    public class QuestionAsked : IDomainEvent
    {
        public Question Question { get; set; }
    }
}
