﻿using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain.Events
{
    public class QuizFinished : IDomainEvent
    {
        public Quiz Quiz { get; set; }
        public Player Player { get; set; }
        public int Score { get; set; }
    }
}