﻿using QuizOfTheDay.Infrastructure;

namespace QuizOfTheDay.Domain.Events
{
    public class QuizSelected : IDomainEvent
    {
        public Quiz Quiz { get; set; }
    }
}
