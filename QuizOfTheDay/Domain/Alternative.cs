﻿namespace QuizOfTheDay.Domain
{
    public class Alternative
    {
        public string Text { get; private set; }
        public int Score { get; private set; }

        public Alternative(string text, int score, Question question)
        {
            Text = text;
            Score = score;
        }
    }
}